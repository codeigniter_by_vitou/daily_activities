<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>BuildBootstrap export</title>

  <!-- Bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.css';?>">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <form id="daily_spend_form" action="<?php echo base_url().'activitycontroller/store'; ?>" method="POST">
            <h1>Daily Spend </h1>
            <div class="form-group row">
              <label for="example-text-input" class="col-3 col-form-label">Amount<span>*</span></label>
              <div class="col">
                <input class="form-control" type="text" name="amount"  value="<?php echo set_value('amount'); ?>" id="amount" placeholder="0.00$" data-validation="alphanumeric">
              </div>
            </div>
            <div class="form-group row">
              <label for="exampleTextarea" class="col-3 col-form-label">Detail<span>*</span></label>
              <div class="col">
               <textarea class="form-control" id="detail" rows="3" placeholder="Specific what you spend for" data-validation="required" name=""><?php echo set_value('detail'); ?></textarea >
               <br>
             </div>
           </div>
            <div class="form-group row">
                <label class="col-3"></label>
                <div class="col">
                    <input type="submit" class="btn btn default" name="" value="#submit">
                </div>
            </div>
         </form>
       </div>
     </div>
   </div>
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url().'assets/jquery/jquery.min.js'; ?>"></script>
   <!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="<?php echo base_url().'assets/js/bootstrap.js'; ?>"></script>
   <script src="<?php echo base_url().'assets/js/jquery.form-validator.min.js'; ?>"></script>
   <script type="text/javascript">
        $(function(){
          $.validate();
          $('body').click('#submit',function(){
              var amount = $('#amount').val();
              var detail = $('#detail').val();
              if (amount || detail == '') {
                    inlineErrorMessageCallback:  function($detail, errorMessage, config) {
                   if (errorMessage) {
                    customDisplayInlineErrorMessage($input, errorMessage);
                   } else {
                    customRemoveInlineError($detail);
                   }
                   return false; // prevent default behaviour
                }
              }
          });
        });
   </script>
 </body>
 </html>